#!/bin/bash
rm -rf /tmp/lolligo
mkdir -p /tmp/lolligo
cp -pvr * /tmp/lolligo/
rm -rf /tmp/lolligo/.git

cd /tmp/lolligo/
tar -czf lolligo.tar.gz *
scp lolligo.tar.gz lisa:/tmp/
#ssh lisa 'cd /var/www/lolligo.net/htdocs && rm -rf * && tar -xf /tmp/lolligo.tar.gz'

echo "Website is now up-to-date!"
